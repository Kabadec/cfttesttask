namespace CftTestTask.Games;

public interface IGame
{
    string Description { get; }
    void Play();
}