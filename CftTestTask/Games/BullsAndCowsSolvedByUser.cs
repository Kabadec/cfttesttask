using System.Text;
using CftTestTask.BullsAndCowsModels;

namespace CftTestTask.Games;

public class BullsAndCowsSolvedByUser : IGame
{
    private readonly BullsAndCowsModel m_BullsAndCowsModel = new BullsAndCowsModel();
    
    public string Description => "Быки и Коровы, разгадывает пользователь";
    
    public void Play()
    {
        m_BullsAndCowsModel.Restart();
        Console.WriteLine("Компутер загадал число. Попробуйте угадать.");
        Console.WriteLine();
        while (true)
        {
            Console.Write("Введите число:");
            var readUserNumber = ReadUserNumber();
            var solveResult = m_BullsAndCowsModel.TrySolve(readUserNumber);
            if (solveResult.IsSolve)
            {
                Console.WriteLine("Поздравляем! Вы угадали число!");
                Console.ReadLine();
                break;
            }
            
            Console.Write("Вы не угадали, попробуйте ещё раз.");
            Console.WriteLine(CreateClueDescription(solveResult.Clue));
        }
    }

    private int ReadUserNumber()
    {
        while (true)
        {
            var readValue = Console.ReadLine();
            if (int.TryParse(readValue, out var result) && m_BullsAndCowsModel.ValidateNumber(result))
            {
                return result;
            }

            Console.Write("Неверный ввод. Введите четырёхзначное число с неповторяющимися цифрами:");
        }
    }

    private string CreateClueDescription(Clue clue)
    {
        var clueBuilder = new StringBuilder();
        clueBuilder.Append(" Подсказка: ");
        var numCows = clue.NumCows;
        var numBulls = clue.NumBulls;
        clueBuilder.Append($"коров - {numCows}");
        clueBuilder.Append(" ");
        clueBuilder.Append($", быков - {numBulls}");
        clueBuilder.Append(".");
        return clueBuilder.ToString();
    }
}