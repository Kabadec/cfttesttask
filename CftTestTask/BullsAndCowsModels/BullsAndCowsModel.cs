namespace CftTestTask.BullsAndCowsModels;

public class BullsAndCowsModel
{
    private const int m_NumDigits = 4;
    
    private List<int> m_SecretSeparatedNumber;

    public BullsAndCowsModel()
    {
        m_SecretSeparatedNumber = GenerateNewSecretSeparatedNumber();
    }

    public void Restart()
    {
        m_SecretSeparatedNumber = GenerateNewSecretSeparatedNumber();
    }

    public SolveResult TrySolve(int userNumber)
    {
        var userSeparatedNumber = SeparateNumber(userNumber);
        if (!ValidateNumber(userSeparatedNumber))
        {
            return new SolveResult(
                false,
                new Clue(0, 0));
        }
        
        var isSolve = CompareSeparatedNumber(m_SecretSeparatedNumber, userSeparatedNumber);

        return new SolveResult(
            isSolve,
            new Clue(
                GetNumCows(userSeparatedNumber),
                GetNumBulls(userSeparatedNumber)));
    }

    public bool ValidateNumber(int number)
    {
        return ValidateNumber(SeparateNumber(number));
    }

    private bool ValidateNumber(IReadOnlyList<int> separatedNumber)
    {
        if (separatedNumber.Count != m_NumDigits)
        {
            return false;
        }

        return separatedNumber.Count == separatedNumber.Distinct().Count();
    }

    private List<int> GenerateNewSecretSeparatedNumber()
    {
        var availableDigits = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        var random = new Random();
        var secretSeparatedNumber = new List<int>();
        
        var firstRandomIndex = random.Next(1, availableDigits.Count);
        secretSeparatedNumber.Add(availableDigits[firstRandomIndex]);
        availableDigits.RemoveAt(firstRandomIndex);

        for (var i = 0; i < m_NumDigits - 1; i++)
        {
            var randomIndex = random.Next(0, availableDigits.Count);
            secretSeparatedNumber.Add(availableDigits[randomIndex]);
            availableDigits.RemoveAt(randomIndex);
        }

        return secretSeparatedNumber;
    }

    private List<int> SeparateNumber(int number)
    {
        var separatedNumber = new List<int>();
        while(number > 0)
        {
            separatedNumber.Add(number % 10);
            number /= 10;
        }
        
        separatedNumber.Reverse();
        return separatedNumber;
    }

    private bool CompareSeparatedNumber(IReadOnlyList<int> firstNumber, IReadOnlyList<int> secondNumber)
    {
        if (firstNumber.Count != secondNumber.Count)
        {
            return false;
        }

        return !firstNumber.Where((t, i) => t != secondNumber[i]).Any();
    }

    private int GetNumCows(IReadOnlyList<int> digitsOfNumber)
    {
        var numCows = 0;
        for (var i = 0; i < digitsOfNumber.Count; i++)
        {
            var currentDigit = digitsOfNumber[i];
            var numEqualDigits = m_SecretSeparatedNumber.Count(digit => digit == currentDigit);
            if (currentDigit != m_SecretSeparatedNumber[i] && numEqualDigits >= 1)
            {
                numCows++;
            }
        }

        return numCows;
    }

    private int GetNumBulls(IReadOnlyList<int> digitsOfNumber)
    {
        var numBulls = 0;
        for (var i = 0; i < digitsOfNumber.Count; i++)
        {
            if (digitsOfNumber[i] == m_SecretSeparatedNumber[i])
            {
                numBulls++;
            }
        }

        return numBulls;
    }
}