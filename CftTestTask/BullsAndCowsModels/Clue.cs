namespace CftTestTask.BullsAndCowsModels;

public class Clue
{
    public int NumCows { get; }
    public int NumBulls { get; }

    public Clue(int numCows, int numBulls)
    {
        NumCows = numCows;
        NumBulls = numBulls;
    }
}