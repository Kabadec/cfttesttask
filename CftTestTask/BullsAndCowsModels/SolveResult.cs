namespace CftTestTask.BullsAndCowsModels;

public class SolveResult
{
    public bool IsSolve { get; }
    public Clue Clue { get; }

    public SolveResult(bool isSolve, Clue clue)
    {
        IsSolve = isSolve;
        Clue = clue;
    }
}