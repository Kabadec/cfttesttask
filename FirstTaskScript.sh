#!/bin/bash

# Получение параметров командной строки
read -p "Введите директорию:" directory
read -p "Введите расширение(без точки):" extension
extension=".$extension"

# Переход в выбранную директорию
cd "$directory" || {
    echo "Невозможно перейти в указанную директорию."
    exit 1
}

# Удаление файлов с заданным расширением
find . -type f -name "*$extension" -type f -delete

echo "Удаление файлов с расширением $extension в директории $directory завершено."